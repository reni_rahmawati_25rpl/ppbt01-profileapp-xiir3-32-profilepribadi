package id.sch.smktelkom_mlg.profilerenitugas

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button

class KeluargaFragment : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_keluarga_fragment)

        supportActionBar!!.title = "Detail Family"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val button = findViewById<Button>(R.id.btn_galery)
        button.setOnClickListener {
            val intent = Intent(this, FotoKeluarga::class.java)
            startActivity(intent)
        }
    }
}
