package id.sch.smktelkom_mlg.profilerenitugas

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class BiodatakuFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.activity_biodataku_fragment, container, false)
        val btnfamily = view.findViewById<Button>(R.id.btn_family)
        btnfamily.setOnClickListener {
            val intent = Intent(view.context, KeluargaFragment::class.java)
            view.context.startActivity(intent)
        }
        return view
    }


}
